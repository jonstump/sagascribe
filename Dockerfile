############################################################
# Dockerfile to build SagaScribe container images
# Based on Ubuntu
############################################################

# Set base image from Ubuntu 20.04 LTS
FROM ubuntu
ARG DEBIAN_FRONTEND=noninteractive

# Set version label and info
ARG BUILD_DATE
ARG VERSION
ARG CERTBOT_VERSION
LABEL build_version='sagascribe version:- ${VERSION} Build-date:- ${BUILD_DATE}'
LABEL maintainer='jonstump'

################## BEGIN INSTALLATION ######################
# making sure box is up to date:
RUN \
  echo "*** install initial packages ***" && \
  apt-get update && \
  apt-get -y upgrade && \
  apt-get install -y \
    curl \
    apt-utils \
    neovim \
    wget \
    htop \
    git

RUN echo "Etc/UTC" > /etc/timezone

################## INSTALL NODE & NPM ######################
RUN \
  echo "*** install nodejs ***" && \
    curl -sL https://deb.nodesource.com/setup_11.x

RUN apt-get install -y nodejs \
      npm

################## CODEBASE INSTALL   ######################
RUN \
  echo "*** install codimd/sagascribe ***" && \
    git clone https://github.com/jonstump/codimd-saga.git

WORKDIR codimd-saga

RUN git checkout main && \
  npm install && \
  npm audit fix && \
  bin/setup && \
  npm run build

EXPOSE 3000

CMD ["npm","start"]
