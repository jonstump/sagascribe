# Epicodus Capstone Notes

## Intro

The goal of this project is to implement the opensource software [Codimd](https://github.com/hackmdio/codimd) into a personal Kubernetes cluster with some modifications for my friends who play Tabletop games, such as Dungeons and Dragons, so that they can have collaborative notes for their various games. Long term it is my hope to have a helm chart that launches this software along with some other supporting software for our online games.

This is also so that I may learn DevOps tools such as Kubernetes and Helm for the purpose of searching for DevOps jobs when I am done with Epicodus.

You can find [detailed notes of my progress here](https://hackmd.io/@anELuJrYSSqX_IRVBG1eWQ/HJS2vAzuO)

[A link to my capstone purposal is here](https://docs.google.com/document/d/1rXXX_MddH-C17QYdrYOpNWFybQRp9hkMOhIrg_EnvSg/edit?usp=sharing)

[Second Repo with CodiMD Code](https://github.com/jonstump/codimd-saga.git)

[Docker image repo](https://hub.docker.com/repository/docker/jmstump/sagascribe-notes)

---

## Technologies Used

* Linux/Ubuntu
* AWS
* Kubernetes
* JavaScript
* Helm
* Vagrant
* Python/pip
* Docker

---

## Goals/ToDos

### New Priority Todos based on work from 05/19/2021

* [ ] Look into microk8s vs minikube for local hosting
     * [ ] How to setup DNS for both
     * [ ] How to setup persistent volume for both
     * [ ] Pro / Cons of each tool

### Previous Todo list

* [x] Install Minikube and associated tools locally for dev environment
* [x] Install Vagrant for testing cluster installs locally for development
* [x] Update NS on domain host Joker
    * [x] Double check that the NS have updated after some time since they may be cached.
* [x] Install Docker
* [x] Turn repo into working tree for easier code editing.
* [ ] Make edits to CodiMD to fit the theme of gaming
    * [X] Branding rename to Saga Scribe for my nerd friends and to use a custom domain
    * [ ] Implement a random dice roller in case anyone forgets their dice and needs some while taking notes
    * [X] Sit with largest note takers to see what other features related to gaming they may want to see implmented
    ~~Understand Config.json for various setup of app~~
    ~~Look into various sign in methods included. May need to do some digging to get all of the different auths setup for users.~~
  * Docker compose file takes care of environment variables that would be in the config file.
* [x] Package CodiMD into a Docker image
    * [X] Create Dockerfile
    * [x] Make sure Dockerfile can launch
* [ ] Deploy image to kubernetes cluster
    * [x] Find out how to convert docker compose file for K8s use.
    * [x] Test with another image on the weekend
* [ ] Setup SSH for aws instance
* [ ] Manually launch cluster locally via minikube
* [x] Learn how to connect Postgres backend as part of image
* [ ] Setup HTTPS for the live environment
* [ ] Learn how to deploy package via Helm for easy setup for anyone
    * [ ] Read up on creating Helm packages
* [x] Sign up for AWS free tier
    * [x] Investigate how much AWS is after the first year
    * [ ] Possibly setup future plan to migrate to another service after the free year is up depending on pricing.

### Stretch Goals:
* [ ] Install [Jitsi on kubernetes](https://github.com/jitsi-contrib/jitsi-kubernetes) if the cluster can handle it and notes at the same time.
* [ ] Install [OONLYOFFICE Docs for Kubernetes](https://github.com/ONLYOFFICE/Kubernetes-Docs)

---

## Bugs

Won't launch on local Kubernetes cluster. Currently getting two different errors.
1. DNS related on minikube
2. persistent storage on microk8s

---

## License

[AGPLv3](/LICENSE)
